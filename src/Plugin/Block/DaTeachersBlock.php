<?php

namespace Drupal\da_teachers_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'DaTeachersBlock' block.
 *
 * @Block(
 *  id = "da_teachers_block",
 *  admin_label = @Translation("Da teachers block"),
 * )
 */
class DaTeachersBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field_da_teachers_url' => $this->t('/es/master/profesorado'),
      'field_da_teachers_text'=> $this->t('Profesorado del máster'),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['field_da_teachers_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('url'),
      '#description' => $this->t('Destination url'),
      '#default_value' => $this->configuration['field_da_teachers_url'],
      '#maxlength' => 128,
      '#size' => 64,
      '#weight' => '0',
    ];

    $form['field_da_teachers_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Linked text'),
      '#default_value' => $this->configuration['field_da_teachers_text'],
      '#maxlength' => 128,
      '#size' => 64,
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['field_da_teachers_url'] = $form_state->getValue('field_da_teachers_url');
    $this->configuration['field_da_teachers_text'] = $form_state->getValue('field_da_teachers_text');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['da_teachers_block_field_da_teachers_url']['#markup'] =  $this->configuration['field_da_teachers_url'];
    $build['da_teachers_block_field_da_teachers_text']['#markup'] = '<p>' . $this->configuration['field_da_teachers_text'] . '</p>';

    return $build;
  }

}
