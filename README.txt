Adds custom block with two configurable fields:

- url (textfield)
- text (textfield)

It is used to show a custom url as a block.
The markup can be themed by using block--da-teachers-block.html.twig in teh active theme.
